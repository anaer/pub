// ==UserScript==
// @name 			BaiduPan_password_autofill
// @description		网盘自动填写提取密码【增强版】+网盘超链接与提取码融合。
// @author			极品小猫
// @namespace   	http://www.cnblogs.com/hkmhd/
//@updateURL https://bitbucket.org/anaer/pub/raw/master/monkey/BaiduPan_password_autofill.user.js
// @homepage		https://greasyfork.org/scripts/13463
// @supportURL		https://greasyfork.org/scripts/13463/feedback
// @version			2.3.2
// @update			2016.08.03
// 
// 支持的网盘
// @include     	http://pan.baidu.com/share/init?*
// @include     	http://yun.baidu.com/share/init?*
// @include     	/^https?://.+\.yunpan.cn/lk//
// @include     	http://*
// @include			https://*
// 
// 白名单
// @exclude			/https?://www.baidu.com/(?:s|baidu)\?/
// @exclude			/https?://tieba.baidu.com/
// @exclude			/https?://eyun.baidu.com/
// @exclude			https://*.evernote.com/*
// @exclude			https://*.yinxiang.com/*
// @exclude			http*://s.360.cn/*
// @exclude			http*://*.yunpan.cn/resource/html/proxy.html
// @exclude         https://www.hscloud.cn/*
// @require			http://code.jquery.com/jquery-2.1.4.js
// @grant			unsafeWindow
// @encoding		utf-8
// @run-at			document-end
// ==/UserScript==

var urls=location.href;
var hash=location.hash;
var host=location.hostname.replace(/^www\./i,'').toLowerCase();
unsafeWindow.eve = Event;

/*
更新日志
2.3.2 【2016.08.04】
1、恢复提取码中的“密码”关键字（适用于：心海e站）

2.3.1 【2016.08.03】
1、增加微云网盘提取码支持（匹配规则来自原作者 Jixun.Moe）
2、修正提取码兼容问题
3、修正重复添加提取码

2.3.0 【2016.08.01】
1、移除对金山快盘、新浪云盘的支持
2、百度企业云盘不追加验证码
3、受贴吧页面跳转影响，暂时不支持贴吧的密码提取，已将贴吧加入白名单
4、提升链接&密码融合的成功率	—— A 标签绑定函数更改为 body 点击事件监听（根据原作者 Jixun.Moe 的建议）
5、找不到密码时的遍历方式更改（感谢 10139 - mudoo 的建议）
6、支持密码放在换行表格中的提取
*/

var site = {
  'yunpan.cn': {
    chk:  /^[a-z0-9]{4}$/,
    code: '.pwd-input',
    btn:  '.submit-btn'
  },
  'baidu.com': {
    chk:  /^[a-z0-9]{4}$/,
    //code: '#accessCode',
    //btn:  '#submitBtn'
      // mac下 调整
    code: 'input[type=text]',
    btn: 'a.g-button.g-button-blue-large'
  },
  'weiyun.com': {
    chk: /^[a-z0-9]{4}$/i,
    code: '#outlink_pwd',
    btn:  '#outlink_pwd_ok'
  },
  'codeRule':/(?:提取|访问|密|艾)[码碼]?\s*[:： ]?\s*([a-z\d]{4})/i
};

var hostName = location.host.match(/\w+\.\w+$/)[0].toLowerCase();	//提取当前网站主域名（网盘填充密码用）
var conf = site[hostName];											//设置主域名

var HostArr = [];									//生成域名数组
for(var i in site) HostArr.push(i);					//插入域名对象数组
var HostExp = new RegExp(HostArr.join("|"),'i');	//生成校验超链接的正则

if(conf&&!/zhidao.baidu.com/i.test(host)){	//网盘页面填密码登录
	// 抓取提取码
	var sCode = hash.slice(1).trim();
	
	// 调试用，检查是否为合法格式
	if (!conf.chk.test(sCode)) {
		console.log('没有 Key 或格式不对');
	} else {
		console.log ('抓取到的提取码: %s', sCode);
	}

		// 加个小延时
		setTimeout (function () {
			// 键入提取码并单击「提交」按钮，报错不用理。
			var codeBox = $(conf.code),
				btnOk = $(conf.btn);

			codeBox.val(sCode);		//填写验证码
			
			if (conf.preSubmit)
				if (conf.preSubmit (codeBox, btnOk))
					return ;

			btnOk.click();
		}, 10);
} else {
  $('body').on('click', 'a', function () {
    var target=this;
    
    if(this.hash) return;   //链接已有提取码时，不再重复提取
    //var target=event.target.tagName==='A'?event.target:event.target.parentNode;
    if(HostExp.test(this.href)&&!/(?:eyun|tieba)\.baidu\.com/i.test(this.href)){	//正则校验匹配的网盘，豁免 百度企业云盘
      if(site['codeRule'].test(target.textContent)){
        console.log('在当前超链接的对象中查找密码');
        target.href+='#'+extCode(target);
      } else if(target.nextSibling&&site['codeRule'].test(target.nextSibling.textContent)){
        console.log('密码在超链接后面的兄弟元素中',target.nextSibling.textContent);
        if(!/#/i.test(target.href)) target.href+='#'+extCode(target.nextSibling);
      } else if(site['codeRule'].test(target.parentNode.textContent)){
        console.log('从父对象中查找密码');
        if(!/#/i.test(target.href)) target.href+='#'+extCode(target.parentNode);
      } else {
        var i = 0,
            maxParent = 5,	//向上遍历的层级
            parent = target;
        while(i<maxParent) {
          i++;									//遍历计数
          parent = parent.parentNode;			//取得父对象
          console.log('遍历上级目录查找密码：'+ i);
          if(parent.tagName=="TR") {				//如果父对象是表格，则从表格中提取密码
            if(site['codeRule'].test(parent.nextElementSibling.textContent)) {
              parent=parent.nextElementSibling;
              //console.log('表格中查找密码成功！',parent);
              target.href+='#'+extCode(parent);
              break;
            }
          } else if(site['codeRule'].test(parent.textContent)) {		//否则按照常规方式提取密码
            console.log('向上遍历查找密码成功！');
            target.href+='#'+extCode(parent);
            break;
          }
          if(parent==document.body) break;		//如果已经遍历到最顶部，停止遍历
        }
        
        //旧的从父对象中遍历方式
        //console.log('从 document.body 中查找密码');
        //if(!/#/i.test(target.href)) target.href+='#'+extCode(document.body);
      }
      console.log(site['codeRule']);
    }
  });
}

function extCode(obj){
	text=obj.textContent.trim();
	var rule=new RegExp('(?:提取|访问)[码碼]?\s*[:： ]?\\s*([a-z\\d]{4})','i');
	return rule.test(text)?text.match(rule)[1]:text.match(site['codeRule'])[1];	//首先尝试使用 提取码|访问码 作为密码匹配的关键字，无效时则使用更完整的匹配规则
}


