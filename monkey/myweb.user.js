// ==UserScript==
// @name        myweb
// @name:zh-CN  自用服务
// @namespace   自用
// @description  自动记录浏览器访问记录
// @version     0.0.1
// @grant       none
//@updateURL https://bitbucket.org/anaer/pub/raw/master/monkey/myweb.user.js
// @exclude     http://127.0.0.1*
// @exclude     http://10.139.104.41*
// @exclude http://www.baidu.com/s*
// @exclude https://www.baidu.com/s*
// @exclude http://fm.baidu.com*
// @exclude http://pan.baidu.com*
// @exclude http://m.baidu.com*
// @exclude http://www.jd.com/*
// @exclude http://download.csdn.net*
// @exclude http://open.hs.net/*
// @exclude http://sonar.oschina.net/*
// @exclude https://open.hs.net/*
// @exclude https://light.hs.net/*
// @exclude http://www.bilibili.com/*
// @exclude https://www.bilibili.com/*
                                     * 
// @require http://libs.baidu.com/jquery/1.9.1/jquery.min.js
// ==/UserScript==
jQuery.noConflict();
function print(msg) {
  console.log(msg);
}
var title = encodeURI(document.title);
var url = window.location.href;
var domain = document.location.host;
//print('title:' + title);
//print('url:' + url);
//print('domain:' + domain);
jQuery.ajax({
  type: 'POST',
  url: 'http://127.0.0.1:6789/myweb/site/insert1.json',
  data: {
    name: title,
    url: url,
    domain: domain
  },
  dataType: 'jsonp',
  //jsonpCallback: 'jsonpCallback',
  success: function (data) {
    print('记录站点成功:' + JSON.stringify(data));
  }
});
