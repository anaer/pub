// ==UserScript==
// @name mouse
// @name:zh-CN 鼠标点击特效
// @namespace Violentmonkey Scripts
// @match *://*/*
// @version 20171114.1444
// @updateURL https://bitbucket.org/anaer/pub/raw/master/monkey/mouse.js
// @downloadURL https://bitbucket.org/anaer/pub/raw/master/monkey/mouse.js
// @grant none
// ==/UserScript==

/* 鼠标点击特效 */
var a_idx = 0;
jQuery(document).ready(function($) {
    $("body").click(function(e) {

// 续秒
var msg="+" + Math.round(Math.random()*9+1) + "s";      
var $i = $("<span/>").text(msg);

// 24字 
//var a = new Array("富强", "民主", "文明", "和谐", "自由", "平等", "公正" ,"法治", "爱国", "敬业", "诚信", "友善");   
//var $i = $("<span/>").text(a[a_idx]);
//        a_idx = (a_idx + 1) % a.length;

var x = e.pageX,
        y = e.pageY;
        $i.css({
"z-index": 999999999999999999999999999999999999999999999999999999999999999999999,
"top": y,
"left": x + 15,
"position": "absolute",
"font-weight": "bold",
"color": "#ff6651"
        });
        $("body").append($i);
        $i.animate({
"top": y - 1000,
"opacity": 0
        },
        10000,
function() {
            $i.remove();
        });
    });
});
